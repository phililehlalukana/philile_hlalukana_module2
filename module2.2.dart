void main() {
  List<Map<String, dynamic>> winners = [
    {"App": 'FNB Banking App', "Year": 2012},
    {"App": 'SnapScan', "Year": 2013},
    {"App": 'LIVE Inspect', "Year": 2014},
    {"App": 'Wumdrop', "Year": 2015},
    {"App": 'Domestly', "Year": 2016},
    {"App": 'Shyft', "Year": 2017},
    {"App": 'Khula ecosystem', "Year": 2018},
    {"App": 'Naked Insurance', "Year": 2019},
    {"App": 'EasyEquities', "Year": 2020},
    {"App": 'Ambani Africa ', "Year": 2021},
  ];
  List<String> App = winners.fold<List<String>>(
      [], (prev, element) => List.from(prev)..add(element['App']));
  print(App);

  var apps = [
    "FNB Banking App",
    "SnapScan",
    "LIVE Inspect",
    "Wumdrop",
    "Domestly",
    "Shyft",
    "Khula ecosystem",
    "Naked Insurance",
    "EasyEquities",
    "Ambani Africa"
  ];

  print(apps.sublist(5, 7));
}
