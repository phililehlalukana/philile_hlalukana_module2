void main() {
  var theapp = new Apps();

  theapp.name = 'Ambani Africa';
  theapp.category = 'Education';
  theapp.year = 2021;
  theapp.developper = 'Mukundi Lambani';
}

class Apps {
  String? name;
  String? category;
  int? year;
  String? developper;

  void printAppsbyname() {
    print("The name of an App is $name");
    print("The category of an App is $category");
    print("The year of which the app won a first prize is $year");
    print("The app was developped on the year $year");
  }
}

void nameOttheApp() {
  String name = "Ambani Africa";

  print(name.toUpperCase());
}
